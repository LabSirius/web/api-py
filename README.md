<div align="center">

# API Sirius

![coverage](./coverage.svg) [![MongoDB Status](https://img.shields.io/badge/PostgreSQL-13.0-blue.svg?longCache=true&style)](https://www.postgresql.org/) [![NPM Status](https://img.shields.io/badge/Python-3.9-blue.svg?longCache=true&style)](https://nodejs.org/) ![code style](https://img.shields.io/badge/code%20style-black-000000.svg)

API to Sirius's services

</div>

## Requirements/Dependencies

- [Python](https://nodejs.org/)
- [PostgreSQL](https://www.postgresql.org/)

## Installation

Install dependencies and extra packages

```bash
pip install requirements.txt
```

## Testing

Using *nose2*

```
nose2 -v -c unittest.cfg --fail-fast
```

## Run

### Develop

```
uvicorn app.main:app --reload
```

---

## RESTful API

| URL | Method | Description | Params |
| --- | --- | --- | --- |
| `/users` | GET | Get a list of users using search filters |  |
| `/users/:id` | GET |  |  |
| `/users` | POST |  |  |
| `/users/:id` | PUT |  |  |
| `/users/:id` | DELETE |  |  |

___
<div align="center">

Collaborators

|[<img src="https://avatars3.githubusercontent.com/u/14989202?s=400&v=4" width="100px;"/><br /><sub><b>Alejandro E. Rendon</b></sub>](https://github.com/aerendon)|
| :---: |

</div>
