from fastapi import Depends, FastAPI, Header, HTTPException, Request, Response

from .routers import user

from . import models, schemas
from app.database import engine, SessionLocal

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


@app.middleware("http")
async def db_session_middleware(request: Request, call_next):
    response = Response("Internal server error", status_code=500)
    try:
        request.state.db = SessionLocal()
        response = await call_next(request)
    finally:
        request.state.db.close()
    return response


async def get_token_header(x_token: str = Header(...)):
    if x_token != "fake-super-secret-token":
        raise HTTPException(status_code=400, detail="X-Token header invalid")


@app.get("/")
async def root():
    return {"message": "Hello World"}


app.include_router(user.router, prefix="/users", tags=["users"])
