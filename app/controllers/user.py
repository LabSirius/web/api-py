from fastapi import HTTPException
from app.managers import user as user_manager


class UserController:
    @staticmethod
    def create_user(user, db):
        db_user = user_manager.get_user_by_email(db, email=user.email)

        if db_user:
            raise HTTPException(status_code=400, detail="Email already registered")
        return user_manager.create_user(db=db, user=user)

    @staticmethod
    def read_users(skip, limit, db):
        users = user_manager.get_users(db, skip=skip, limit=limit)

        return users

    @staticmethod
    def read_user(user_id, db):
        db_user = user_manager.get_user(db, user_id=user_id)

        if db_user is None:
            raise HTTPException(status_code=404, detail="User not found")
        return db_user
