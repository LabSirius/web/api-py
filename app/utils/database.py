from fastapi import Request

from app.database import SessionLocal, engine


def get_db(request: Request):
    return request.state.db
