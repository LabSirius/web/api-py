from fastapi import Depends, APIRouter
from sqlalchemy.orm import Session
from typing import List

from app import schemas
from app.controllers.user import UserController
from app.utils.database import get_db

router = APIRouter()


@router.post("/", response_model=schemas.User)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    user_controller = UserController()

    return user_controller.create_user(user, db)


@router.get("/", response_model=List[schemas.User])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    user_controller = UserController()

    return user_controller.read_users(skip, limit, db)


@router.get("/{user_id}", response_model=schemas.User)
def read_user(user_id: int, db: Session = Depends(get_db)):
    user_controller = UserController()

    return user_controller.read_user(user_id, db)
